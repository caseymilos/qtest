<?php


class Person {

	private $FirstName;
	private $LastName;
	private $DateOfBirth;
	private $BirthPlace;
	private $Address;
	private $Gender;
	private $Height;
	private $Weight;


	public function __construct($FirstName, $LastName, $DateOfBirth, $BirthPlace = null, $Address = null, $Gender = null, $Height = null, $Weight = null) {
		$this->FirstName = $FirstName;
		$this->LastName = $LastName;
		$this->DateOfBirth = $DateOfBirth;
		$this->BirthPlace = $BirthPlace;
		$this->Address = $Address;
		$this->Gender = $Gender;
		$this->Height = $Height;
		$this->Weight = $Weight;
	}

	public function __toString() {
		return "Hi my name is " . $this->FirstName . " " . $this->LastName . " and i am " . $this->getAge() . " years old ";
	}

	public function getAge() {
		return DateTime::createFromFormat('d-m-Y', $this->DateOfBirth)->diff(new DateTime('now'))->y;
	}

	/**
	 * @return string
	 */
	public function getFirstName() {
		return $this->FirstName;
	}

	/**
	 * @param string $FirstName
	 */
	public function setFirstName($FirstName) {
		$this->FirstName = $FirstName;
	}

	/**
	 * @return string
	 */
	public function getLastName() {
		return $this->LastName;
	}

	/**
	 * @param string $LastName
	 */
	public function setLastName($LastName) {
		$this->LastName = $LastName;
	}

	/**
	 * @return string
	 */
	public function getDateOfBirth() {
		return $this->DateOfBirth;
	}

	/**
	 * @param string $DateOfBirth
	 */
	public function setDateOfBirth($DateOfBirth) {
		$this->DateOfBirth = $DateOfBirth;
	}

	/**
	 * @return string
	 */
	public function getBirthPlace() {
		return $this->BirthPlace;
	}

	/**
	 * @param string $BirthPlace
	 */
	public function setBirthPlace($BirthPlace) {
		$this->BirthPlace = $BirthPlace;
	}

	/**
	 * @return string
	 */
	public function getAddress() {
		return $this->Address;
	}

	/**
	 * @param string $Address
	 */
	public function setAddress($Address) {
		$this->Address = $Address;
	}

	/**
	 * @return string
	 */
	public function getGender() {
		return $this->Gender;
	}

	/**
	 * @param string $Gender
	 */
	public function setGender($Gender) {
		$this->Gender = $Gender;
	}

	/**
	 * @return float
	 */
	public function getHeight() {
		return $this->Height;
	}

	/**
	 * @param float $Height
	 */
	public function setHeight($Height) {
		$this->Height = $Height;
	}

	/**
	 * @return float
	 */
	public function getWeight() {
		return $this->Weight;
	}

	/**
	 * @param float $Weight
	 */
	public function setWeight($Weight) {
		$this->Weight = $Weight;
	}
}

$person = new Person("Milos", "Stojkovic", "04-09-1990");

echo $person;

echo "<br/>";

$person2 = new Person("Petar", "Petrovic", "04-09-1991", "Nis", "Prvomajska", "Muski", 190.90);

echo $person2;
